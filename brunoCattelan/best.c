#include <omp.h>
#include "lib/hpcelo.h"
#define SIZE 10000
double A1[10001*10001];
double B1[10001*10001];
double C[10001*10001];
  static unsigned int g_seed = 0;
  static inline int fastrand()
  {
    g_seed = (214013*g_seed+2531011);
    return (g_seed>>16)&0x7FFF;
  }
  //  ...



void printMatrix(double* matrix){
  int line, column;
   for (line = 0; line < SIZE; line++) {
      for (column = 0; column < SIZE; column++) {
        printf("%f ",*(matrix+line*SIZE + column));
      }
    printf("\n");
  }
}
void printMatrix2(double** matrix){
  int line, column;
   for (line = 0; line < SIZE; line++) {
      for (column = 0; column < SIZE; column++) {
        printf("%f ",matrix[line][column]);
      }
    printf("\n");
  }
}

int main (int argc, char **argv)
{
  double **A, **B;
  int i,j,line,column,k,m;
  double temp = 0;
  HPCELO_DECLARE_TIMER;
  
  A = (double**)malloc(SIZE*sizeof(double));
  B = (double**)malloc(SIZE*sizeof(double));
  for (i = 0; i < SIZE; i++){
    A[i] = (double*)malloc(SIZE*sizeof(double));
    B[i] = (double*)malloc(SIZE*sizeof(double));
    for (j = 0; j < SIZE; j++){
       A[i][j] = fastrand();
       B[i][j] = fastrand();
    }
  }

  HPCELO_START_TIMER;
#pragma omp parallel for private(line,column)
for ( line = 0; line < SIZE; line++)
{
    for ( column = 0; column < SIZE; column++)
    {
        *(B1+line*SIZE + column) = B[column][line];
        *(A1+line*SIZE + column) = A[line][column];
    }
}

    //printf("A\n");
    //printMatrix(A1);
    //printf("B\n");
    //printMatrix2(B);

#pragma omp parallel for firstprivate(temp) private(i,k,m)
for( i = 0; i < SIZE; i++)
{
    for ( k = 0; k < SIZE; k++)
    {
        temp = 0;
        for ( m = 0; m < SIZE; m++)
        {
            temp = temp + *(A1+i*SIZE + m) * *(B1+k*SIZE + m);
        }
        *(C+i*SIZE + k) = temp;
    }
}
          

 
  HPCELO_END_TIMER;
  HPCELO_REPORT_TIMER;
    for (i = 0; i < SIZE; i++){
    for (j = 0; j < SIZE; j++){
       printf ("%f\n", *(C+i*SIZE + j));
    }
  }
  //printMatrix(C);
  //USED FOR TESTING

  return 0;
}
