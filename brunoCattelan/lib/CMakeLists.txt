#
# This file is part of libhpcelo
#
# libhpcelo is free software: you can redistribute it and/or modify
# it under the terms of the GNU Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# libhpcelo is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Public License for more details.
#
# You should have received a copy of the GNU Public License
# along with libhpcelo. If not, see <http://www.gnu.org/licenses/>.
#
CMAKE_MINIMUM_REQUIRED (VERSION 2.8)

FIND_PACKAGE(MPI REQUIRED)

SET(LIBHPCELO_HEADERS hpcelo.h)
SET(LIBHPCELO_SOURCES hpcelo.c)
ADD_LIBRARY(hpcelo_library SHARED ${LIBHPCELO_SOURCES})
ADD_LIBRARY(hpcelo_library_static STATIC ${LIBHPCELO_SOURCES})
INSTALL(TARGETS hpcelo_library DESTINATION lib)
INSTALL(TARGETS hpcelo_library_static DESTINATION lib)
INSTALL(FILES ${LIBHPCELO_HEADERS} DESTINATION include)

ADD_EXECUTABLE(usage_example usage_example.c)
TARGET_LINK_LIBRARIES(usage_example hpcelo_library_static)

ADD_EXECUTABLE(usage_example_mpi usage_example_mpi.c)
MESSAGE(STATUS ${MPI_C_INCLUDE_PATH})
INCLUDE_DIRECTORIES(usage_example_mpi INTERFACE ${MPI_C_INCLUDE_PATH})
TARGET_LINK_LIBRARIES(usage_example_mpi hpcelo_library_static)
TARGET_LINK_LIBRARIES(usage_example_mpi ${MPI_LIBRARIES})
