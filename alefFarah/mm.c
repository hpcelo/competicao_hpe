#include <sys/time.h>
#include <stdio.h>
#include <string.h>
#include <stddef.h>

static double
hpcelo_gettime()
{
  struct timeval tr;
  gettimeofday(&tr, NULL);
  return (double)tr.tv_sec + (double)tr.tv_usec / 1000000;
}

#define HPCELO_DECLARE_TIMER double hpcelo_t1, hpcelo_t2;
#define HPCELO_START_TIMER  hpcelo_t1 = hpcelo_gettime();
#define HPCELO_END_TIMER    hpcelo_t2 = hpcelo_gettime();
#define HPCELO_REPORT_TIMER {printf("HPCELO:%f\n", hpcelo_t2-hpcelo_t1);}

static unsigned int g_seed = 0;
static inline int
fastrand()
{
  g_seed = (214013*g_seed+2531011);
  return (g_seed>>16)&0x7FFF;
}

#define SIZE 10000
static double a[SIZE][SIZE];
static double b[SIZE][SIZE];
static double t[SIZE][SIZE];
static double ans[SIZE][SIZE];

int
main(int argc, char **argv)
{
  /* Init */
  for (size_t i = 0; i < SIZE; i++)
    for (size_t j = 0; j < SIZE; j++) {
      a[i][j] = (double)fastrand();
      b[i][j] = (double)fastrand();
      ans[i][j] = 0;
    }
  HPCELO_DECLARE_TIMER;
  HPCELO_START_TIMER;
  /* Transpose b */
  for (size_t i = 0; i < SIZE; i++)
    for (size_t j = 0; j < SIZE; j++)
      t[i][j] = b[j][i];
  /* Multiplicate */
#pragma omp parallel for
  for (size_t i = 0; i < SIZE; i++)
    for (size_t j = 0; j < SIZE; j++)
      for (size_t k = 0; k < SIZE; k++)
        ans[i][j] += a[i][k] * t[j][k];
  HPCELO_END_TIMER;
  HPCELO_REPORT_TIMER;
  /* Report */
  for (size_t i = 0; i < SIZE; i++)
    for (size_t j = 0; j < SIZE; j++)
      printf("%f\n", ans[i][j]);
  return 0;
}
