BUILD_TARGETS=$(subst /,,$(wildcard */))
SOURCES=$(shell find . -name '*.[c|h]')
BUILD_EXE=$(patsubst %,%/mm,$(BUILD_TARGETS))
CLEAN_TARGETS=$(patsubst %,%.clean,$(BUILD_TARGETS))
RUN_TARGETS=$(patsubst %,%.run,$(BUILD_TARGETS))

export CC=gcc

all: test run

$(CLEAN_TARGETS):
	./.togglesize.sh 10000
	@echo Cleaning up - $(@:.clean=)
	make -C $(@:.clean=) clean

$(BUILD_EXE): clean $(SOURCES)
	./.togglesize.sh 10000
	@echo Building - $(@:mm=)
	make -C $(@:mm=)

$(RUN_TARGETS): build
	@echo -n "Performing experiment - "
	@cd $(@:.run=); ./mm | head -1 | sed "s/HPCELO/$(@:.run=)/" | tee -a ../times.csv

.PHONY: build clean run test
build: $(BUILD_EXE)
clean: $(CLEAN_TARGETS)
	rm -f times.csv
run: $(RUN_TARGETS)
test: clean
	./.togglesize.sh 10
	@echo Building test - $(@:mm=)
	for d in $(BUILD_EXE:mm=); do cd $$d; make ; cd ..; done
	$(MAKE) -C .test
