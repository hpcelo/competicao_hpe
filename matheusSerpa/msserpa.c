/* Matheus S. Serpa
 * msserpa@inf.ufrgs.br */

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#define size 10000
#define TILE 8
#define UNROLLING 4
#define num_tiles 1250 /* size / TILE */

//next function has been found here:
//http://stackoverflow.com/questions/26237419/faster-than-rand
//all credits to the authors there
static unsigned int g_seed;
static inline int fastrand(){
	g_seed = (214013*g_seed+2531011);
	return (g_seed>>16)&0x7FFF;
}

/* hpcelo_gettime: this function returns the current time in seconds with a microsecond resolution.
* It uses =gettimeofday= call.  */
double hpcelo_gettime(){
	struct timeval tr;
	gettimeofday(&tr, NULL);
	return (double)tr.tv_sec+(double)tr.tv_usec/1000000;
}

#define HPCELO_DECLARE_TIMER double hpcelo_t1, hpcelo_t2;
#define HPCELO_START_TIMER  hpcelo_t1 = hpcelo_gettime();
#define HPCELO_END_TIMER    hpcelo_t2 = hpcelo_gettime();
#define HPCELO_REPORT_TIMER {printf("HPCELO:%f\n", hpcelo_t2-hpcelo_t1);}

int main(){
	int i, j, k, x, y, z;
	double *A, *B, *Bt, *C, sum_step;

	/* Dynamically allocates A, B and C matrices  */
	A  = (double *) malloc(size * size * sizeof(double));
	B  = (double *) malloc(size * size * sizeof(double));
	Bt = (double *) malloc(size * size * sizeof(double));
	C  = (double *) calloc(size * size,  sizeof(double));

	/* Initializes matrices */
	for(i = 0; i < size; ++i)
		for(j = 0; j < size; ++j){
			A[i * size + j] = fastrand();
			B[i * size + j] = fastrand();
		}     

	HPCELO_DECLARE_TIMER
	HPCELO_START_TIMER

	/* matrix transposition */
	#pragma omp parallel for private(i, j)
	for(i = 0; i < size ; ++i)
		for(j = 0; j < size; ++j)
			Bt[i * size + j] = B[j * size + i];


	/* matrix multiplication */
	if(size == 10000){
		#pragma omp parallel for private(i, j, k, x, y, z, sum_step)
		for(i = 0; i < num_tiles; ++i)
			for(j = 0; j < num_tiles; ++j)
				for(k = 0; k < TILE; ++k)
					for(x = 0; x < TILE; ++x){
						sum_step = 0.0;
						for(y = 0; y < num_tiles; ++y)
							for(z = 0; z < TILE; z += UNROLLING){
								sum_step += A[i * TILE * size + y * TILE + k * size + z]     * Bt[j * TILE * size + y * TILE + x * size + z];
								sum_step += A[i * TILE * size + y * TILE + k * size + z + 1] * Bt[j * TILE * size + y * TILE + x * size + z + 1];
								sum_step += A[i * TILE * size + y * TILE + k * size + z + 2] * Bt[j * TILE * size + y * TILE + x * size + z + 2];
								sum_step += A[i * TILE * size + y * TILE + k * size + z + 3] * Bt[j * TILE * size + y * TILE + x * size + z + 3];
							}
						C[i * TILE * size + j * TILE + k * size + x] = sum_step;
					}
	}else{
		#pragma omp parallel for private(i, j, k, sum_step)
		for(i = 0; i < size; ++i)
			for(j = 0; j < size; ++j){
				sum_step = 0.0;
				for(k = 0; k < size; ++k)
					sum_step += A[i * size + k] * Bt[j * size + k];
				C[i * size + j] = sum_step;
			}
	}

	HPCELO_END_TIMER
	HPCELO_REPORT_TIMER

	/* print C to stdout*/
	for(i = 0; i < size ; ++i)
		for(j = 0; j < size; ++j)
			printf("%lf\n", C[i * size + j]);

	/* frees the allocated matrices */
	free(A);
	free(B);
	free(Bt);
	free(C);

	return 0;
}