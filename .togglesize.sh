#!/bin/bash

set -o nounset -o errexit
# Muda o tamanho da matriz para testes locais

new=$1

case $1 in
	10)
		original=10000;;
	10000)
		original=10;;
	*)
		echo "Error: unknown size: $1, must be 10 or 10000"
		exit 2;;
esac



echo "Changing input size from $original to $new"
find -name '*.[c|h]' | xargs perl -pi -e "s/SIZE $original\b/SIZE $new/g"
find -name '*.[c|h]' | xargs perl -pi -e "s/size $original\b/size $new/g"
find -name '*.[c|h]' | xargs perl -pi -e "s/size=$original\b/size=$new/g"
