#include <sys/time.h>
#include <stdio.h>
#include <string.h>
#include <stddef.h>

static unsigned int g_seed = 0;
static inline int
fastrand()
{
  g_seed = (214013*g_seed+2531011);
  return (g_seed>>16)&0x7FFF;
}

#define SIZE 10000
static double a[SIZE][SIZE];
static double b[SIZE][SIZE];
static double ans[SIZE][SIZE];

int
main(int argc, char **argv)
{
  /* Init a*/
  for (size_t i = 0; i < SIZE; i++)
    for (size_t j = 0; j < SIZE; j++) {
      a[i][j] = (double)fastrand();
    }

  /* Init b*/
  for (size_t i = 0; i < SIZE; i++)
    for (size_t j = 0; j < SIZE; j++) {      
      b[i][j] = (double)fastrand();
      ans[i][j] = 0;
    }

  /* Multiplicate */
  for (size_t i = 0; i < SIZE; i++)
    for (size_t j = 0; j < SIZE; j++)
      for (size_t k = 0; k < SIZE; k++)
        ans[i][j] += a[i][k] * b[k][j];
  /* Report */
  for (size_t i = 0; i < SIZE; i++)
    for (size_t j = 0; j < SIZE; j++)
      printf("%f\n", ans[i][j]);
  return 0;
}
