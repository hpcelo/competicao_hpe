#include <stdio.h>
#include <stdlib.h>
#include "omp.h"
#include "hpcelo.h"

#define MATRIX_SIZE 10000

double *matrix_a;
double *matrix_b;
double *transposed_b;
double *matrix_c;

static unsigned int g_seed = 0;
static inline int fastrand() {
    g_seed = (214013*g_seed+2531011);
    return (g_seed>>16)&0x7FFF;
}

int main (int argc, char **argv) {
    unsigned int i, j, k;
    matrix_a = (double*) malloc(sizeof(double) * MATRIX_SIZE * MATRIX_SIZE);
    matrix_b = (double*) malloc(sizeof(double) * MATRIX_SIZE * MATRIX_SIZE);
    transposed_b = (double*) malloc(sizeof(double) * MATRIX_SIZE * MATRIX_SIZE);
    matrix_c = (double*) malloc(sizeof(double) * MATRIX_SIZE * MATRIX_SIZE);
    for (i=0; i<MATRIX_SIZE; i++) {
        for (j=0; j<MATRIX_SIZE; j++) {
            matrix_a[i*MATRIX_SIZE+j] = fastrand();
            matrix_b[i*MATRIX_SIZE+j] = fastrand();
        }
    }

    double sum=0;

    HPCELO_DECLARE_TIMER
    HPCELO_START_TIMER
    #pragma omp parallel shared(matrix_a, matrix_b, matrix_c, transposed_b) private(i, j, k) firstprivate(sum)
    {
        int num_threads = omp_get_num_threads();
        #pragma omp for schedule(static, (MATRIX_SIZE/num_threads)+1)
        for (i=0; i<MATRIX_SIZE; i++) {
            for (j=0; j<MATRIX_SIZE; j++) {
                transposed_b[j*MATRIX_SIZE+i] = matrix_b[i*MATRIX_SIZE+j];
            }
        }

        #pragma omp for schedule(static, (MATRIX_SIZE/num_threads)+1)
        for (i=0; i<MATRIX_SIZE; i++) {
            for (j=0; j<MATRIX_SIZE; j++) {
                for (k=0; k<MATRIX_SIZE; k++) {
                    sum += matrix_a[i*MATRIX_SIZE+k] * transposed_b[j*MATRIX_SIZE+k];
                }

                matrix_c[i*MATRIX_SIZE+j] = sum;
                sum = 0;
            }
        }
    }

    HPCELO_END_TIMER
    HPCELO_REPORT_TIMER

    for (i=0; i<MATRIX_SIZE; i++) {
        for (j=0; j<MATRIX_SIZE; j++) {
            printf("%f\n", matrix_c[i*MATRIX_SIZE+j]);
        }
    }

    return 0;
}
