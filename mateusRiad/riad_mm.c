/*
  Modified Matrix Multiplication example as given on libhpcelo
    by Mateus Riad - mrcsoares@inf.ufrgs.br
	
  Multiplies two square matrices of parameterizable sizes using different
    algorithms, parallel and non-parallel.
*/

/* Args: size alg threads */

#include "hpcelo.h"
#include <string.h>
#include <omp.h>

#define SIZE 10000

static unsigned int g_seed = 0;
static inline int fastrand() {
    g_seed = (214013*g_seed+2531011);
    return (g_seed>>16)&0x7FFF;
}

/* Calculates C = A * B using a parallel algorithm.
  Collapse the first 2 loops into one iteration space.
  Measures time using SAMPLER directives. */
void mmult(double *A, double *B, double *C) {
  int i, j, k;
  double sum;

  HPCELO_DECLARE_TIMER;
  HPCELO_START_TIMER;

  #pragma omp parallel for schedule(static) collapse(2) private (i,j,k,sum) shared (A,B,C)
  for (i = 0; i < SIZE; i++) {
    for (j = 0; j < SIZE; j++) {
      sum = 0;
      for (k = 0; k < SIZE; k++) {
        sum += A[i*SIZE+k] * B[k*SIZE+j];
      }
      C[i*SIZE+j] = sum;
    }
  }
  HPCELO_END_TIMER;
  HPCELO_REPORT_TIMER;
}

/* MAIN */
int main (int argc, char **argv) {
  double *A, *B, *C;
  int i, j , k;

  // intialize all arrays
  A = malloc((long)SIZE*(long)SIZE*sizeof(double*));
  B = malloc((long)SIZE*(long)SIZE*sizeof(double*));
  C = malloc((long)SIZE*(long)SIZE*sizeof(double*));

  for (i=0; i<SIZE; i++) {
    for (j=0; j<SIZE; j++) {
      A[i*SIZE+j] = fastrand();
      B[i*SIZE+j] = fastrand();
    }
  }

  mmult(A, B, C);

  for (i=0; i<SIZE; i++) {
    for (j=0; j<SIZE; j++) {
        printf("%f\n", C[i*SIZE+j]);
    }
  }

  // free all matrices
  hpcelo_free_matrix (A);
  hpcelo_free_matrix (B);
  hpcelo_free_matrix (C);

  return 0;
}
