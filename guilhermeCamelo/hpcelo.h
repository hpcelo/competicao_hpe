#ifndef __HPCELO_H_
#define __HPCELO_H_
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#define HPCELO_DECLARE_TIMER double hpcelo_t1, hpcelo_t2;
#define HPCELO_START_TIMER  hpcelo_t1 = hpcelo_gettime();
#define HPCELO_END_TIMER    hpcelo_t2 = hpcelo_gettime();
#define HPCELO_REPORT_TIMER {printf("HPCELO:%f\n", hpcelo_t2-hpcelo_t1);}

double hpcelo_gettime (void);

double **hpcelo_create_matrix (unsigned long long size);

double **hpcelo_create_matrix_zero (unsigned long long size);

void hpcelo_free_matrix (double **matrix, unsigned long long size);

#endif
