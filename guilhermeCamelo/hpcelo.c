#include "hpcelo.h"

double hpcelo_gettime (void)
{
  struct timeval tr;
  gettimeofday(&tr, NULL);
  return (double)tr.tv_sec+(double)tr.tv_usec/1000000;
}

double **hpcelo_create_matrix (unsigned long long size)
{
  double **matrix = malloc(size*sizeof(double*));
  /* Error checking */
  unsigned long long i, j;
  for (i=0; i < size; i++){
    matrix[i] = malloc(size*sizeof(double));
    for (j = 0; j < size; j++){
      matrix[i][j] = 0; //(double) fastrand();
    }
  }
  return matrix;
}

double **hpcelo_create_matrix_zero (unsigned long long size)
{
  double **matrix = malloc(size*sizeof(double*));
  /* Error checking */
  unsigned long long i, j;
  for (i=0; i < size; i++){
    matrix[i] = malloc(size*sizeof(double));
    for (j = 0; j < size; j++){
      matrix[i][j] = 0;
    }
  }
  return matrix;
}

void hpcelo_free_matrix (double **matrix, unsigned long long size)
{
  unsigned long long i, j;
  for (i=0; i < size; i++){
    free(matrix[i]);
  }
  free(matrix);
  return;
}

