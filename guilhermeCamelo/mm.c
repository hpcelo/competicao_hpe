#include "hpcelo.h"     
#define SIZE 10000
static double A[SIZE][SIZE];
static double B[SIZE][SIZE];
static double C[SIZE][SIZE];
static double B_trans[SIZE][SIZE];   
                                                                              
//next function has been found here: 
//http://stackoverflow.com/questions/26237419/faster-than-rand
//all credits to the authors there                             
static unsigned int g_seed=0;
static inline int fastrand()
{
  g_seed = (214013*g_seed+2531011);
  return (g_seed>>16)&0x7FFF;
}                                                        
 
int main (int argc, char **argv)
{                                                                             
  for (int i=0; i < SIZE; i++)  
    for (int j = 0; j < SIZE; j++)
    {                             
      A[i][j]= (double) fastrand();
      B[i][j]= (double) fastrand();
    }                              
  HPCELO_DECLARE_TIMER;            
  HPCELO_START_TIMER;              
                                   
  for (int i=0; i < SIZE; i++)     
    for (int j = 0; j < SIZE; j++) 
      B_trans[i][j] = B[j][i];     
                                   
  // Perform multiplication with matrix Transpose
#pragma omp parallel for                         
  for (int i=0; i < SIZE; i++){ // iterate lines 
    for (int j = 0; j < SIZE; j++){ // iterate column
      double foo = 0;                                
      for (int k = 0; k < SIZE; k++){ // iterate numbers
        // make multiplication of transpose matrix      
        foo += A[i][k] * B_trans[j][k];                 
      }                                                 
      C[i][j] = foo;                                    
    }                                                   
  }
 //Stops counting time
  HPCELO_END_TIMER;   
                      
  // Report time      
  HPCELO_REPORT_TIMER; 
  for (int i = 0; i < SIZE; i++){
    for (int j = 0; j < SIZE; j++){
      printf ("%f\n", C[i][j]);
    }       
  }         
  return 0; 
}          

